/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oparilov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/25 19:46:49 by oparilov          #+#    #+#             */
/*   Updated: 2023/01/25 19:46:51 by oparilov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	const unsigned char	*tmp;

	tmp = (const unsigned char *)s;
	while (n-- > 0)
	{
		if ((unsigned char)c == *tmp)
		{
			return ((void *)tmp);
		}
		tmp++;
	}
	return (NULL);
}
