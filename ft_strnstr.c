/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oparilov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/25 19:49:35 by oparilov          #+#    #+#             */
/*   Updated: 2023/02/02 17:08:39 by oparilov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include "libft.h"

char	*ft_strnstr(const char *str, const char *sub, size_t nb)
{
	size_t	i;
	size_t	subsize;

	i = 0;
	subsize = ft_strlen(sub);
	if (*sub == '\0')
		return ((char *)str);
	while (*str && i < nb)
	{
		if (!ft_strncmp(str, sub, subsize) && (i + subsize) <= nb)
			return ((char *)str);
		str++;
		i++;
	}
	return (NULL);
}
