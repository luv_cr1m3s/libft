/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oparilov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/25 19:49:28 by oparilov          #+#    #+#             */
/*   Updated: 2023/01/28 18:39:17 by oparilov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s1, char const *set)
{
	char	*result;
	size_t	start;
	size_t	end;

	if (!s1 || !set)
		return (NULL);
	start = 0;
	while (s1[start] && ft_strchr(set, s1[start]))
	{
		start++;
	}
	end = ft_strlen(s1);
	while ((end > start) && ft_strchr(set, s1[end - 1]))
	{
		end--;
	}
	result = malloc((end - start + 1) * sizeof(char));
	if (!result)
	{
		return (NULL);
	}
	ft_strlcpy(result, s1 + start, end - start + 1);
	return (result);
}
