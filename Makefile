NAME = libft.a

SOURCE = 	ft_atoi.c \
					ft_bzero.c \
					ft_calloc.c \
					ft_isalnum.c \
					ft_isalpha.c \
					ft_isascii.c \
					ft_isdigit.c \
					ft_isprint.c \
					ft_itoa.c	\
					ft_memchr.c \
					ft_memcmp.c \
					ft_memcpy.c \
					ft_memmove.c \
					ft_memset.c \
					ft_putchar_fd.c \
					ft_putendl_fd.c \
					ft_putnbr_fd.c \
					ft_putstr_fd.c \
					ft_split.c \
					ft_strchr.c \
					ft_strdup.c \
					ft_striteri.c \
					ft_strjoin.c \
					ft_strlcat.c \
					ft_strlcpy.c \
					ft_strlen.c \
					ft_strmapi.c \
					ft_strncmp.c \
					ft_strnstr.c \
					ft_strrchr.c \
					ft_strtrim.c \
					ft_substr.c \
					ft_tolower.c \
					ft_toupper.c \

BONUS = ft_lstadd_back.c \
				ft_lstadd_front.c \
				ft_lstclear.c \
				ft_lstdelone.c \
				ft_lstiter.c \
				ft_lstlast.c \
				ft_lstmap.c \
				ft_lstnew.c \
				ft_lstsize.c \
 

CC = gcc

FLAGS	=	-c -Wall -Wextra -Werror
ARCHIVE	=	ar rcs
OBJECTS	=	$(SOURCE:.c=.o)
BONUS_OBJ = $(BONUS:.c=.o)
all: $(NAME)

$(NAME):
	@echo "Building object files"
	@$(CC) $(FLAGS) $(SOURCE)
	@echo "Compiling static library"
	@$(ARCHIVE) $(NAME) $(OBJECTS)
	@ranlib $(NAME) 
	@echo "Static library $(NAME) created"

so:
	$(CC) -nostartfiles -fPIC $(FLAGS) $(SOURCE) $(BONUS)
	gcc -nostartfiles -shared -o libft.so $(OBJECTS) $(BONUS_OBJ)

bonus:
	@echo "Building object files"
	@$(CC) $(FLAGS) $(SOURCE) $(BONUS)
	@echo "Compiling static library"      
	@$(ARCHIVE) $(NAME) $(OBJECTS) $(BONUS_OBJ)
	@ranlib $(NAME)                       

.PHONY: all clean fclean re test

fclean: clean
	@rm -f $(NAME)
	@echo "Removed everything what was built"

clean: 
	@rm -f $(OBJECTS) $(BONUS_OBJ) 
	@echo "Removed object files"

re: fclean all

test: all 
	@mv $(NAME) ./test-framework/
	@cp libft.h ./test-framework/
