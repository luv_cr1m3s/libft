/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oparilov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/25 19:47:38 by oparilov          #+#    #+#             */
/*   Updated: 2023/01/28 21:43:10 by oleksii          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_wordcount(char const *s, char c)
{
	int	i;
	int	count;

	i = 0;
	count = 0;
	while (s[i] != '\0')
	{
		while (s[i] == c)
			i++;
		if (s[i] != '\0')
			count++;
		while (s[i] && (s[i] != c))
			i++;
	}
	return (count);
}

int	ft_wordlength(char const *s, char c)
{
	int	count;

	count = 0;
	while (*s != '\0' && *s != c)
	{
		count++;
		s++;
	}
	return (count);
}

char	**ft_split(char const *s, char c)
{
	int		num_words;
	char	**result;
	int		i;
	int		k;
	int		word_length;

	num_words = ft_wordcount(s, c);
	result = ft_calloc(num_words + 1, sizeof(char *));
	if (!result)
		return (NULL);
	i = 0;
	while (i < num_words)
	{
		k = 0;
		while (*s == c)
			s++;
		word_length = ft_wordlength(s, c);
		result[i] = (char *)ft_calloc(word_length, sizeof(char));
		while (*s != '\0' && *s != c)
			result[i][k++] = *s++;
		result[i][k] = '\0';
		i++;
	}
	result[num_words] = NULL;
	return (result);
}
