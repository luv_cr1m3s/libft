/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oparilov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/25 19:50:33 by oparilov          #+#    #+#             */
/*   Updated: 2023/01/28 18:44:40 by oparilov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *str)
{
	size_t	len;
	char	*duplicate;

	len = ft_strlen(str) + 1;
	duplicate = (char *)malloc(len);
	if (duplicate)
	{
		ft_strlcpy(duplicate, str, ft_strlen(str) + 1);
	}
	return (duplicate);
}
