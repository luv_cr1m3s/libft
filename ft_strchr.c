/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oparilov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/25 19:50:11 by oparilov          #+#    #+#             */
/*   Updated: 2023/01/25 19:50:12 by oparilov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *str, int c)
{
	if (c == '\0')
	{
		return ((char *)(str + ft_strlen(str)));
	}
	while (*str)
	{
		if ((unsigned char)*str == c)
		{
			return ((char *)str);
		}
		str++;
	}
	return (NULL);
}
