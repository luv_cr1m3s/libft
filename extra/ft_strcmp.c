int ft_strcmp(const char *str1, const char *str2){
	int tmp = 0;
	
	for(int i = 0; str1[i] != '\0' && str2[i] != '\0'; i++){
		tmp = str1[i] - str2[i];
		if (tmp != 0){
			break;
		}
	}

	return tmp;
}
