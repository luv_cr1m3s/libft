/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oparilov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/25 19:46:55 by oparilov          #+#    #+#             */
/*   Updated: 2023/01/25 19:46:56 by oparilov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *ptr1, const void *ptr2, size_t num)
{
	int				result;
	unsigned char	c1;
	unsigned char	c2;

	result = 0;
	while (num > 0)
	{
		c1 = *(unsigned char *)ptr1;
		c2 = *(unsigned char *)ptr2;
		result = c1 - c2;
		if (result != 0)
		{
			return (result);
		}
		num--;
		ptr1++;
		ptr2++;
	}
	return (result);
}
