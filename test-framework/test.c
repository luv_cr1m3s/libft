#include "libft.h"
#include "unit-test/unity.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>


void setUp(void) {}

void tearDown(void) {}

//Part I

void test_isascii(void) {
  TEST_ASSERT_EQUAL_INT(1, ft_isascii('1'));
  TEST_ASSERT_EQUAL_INT(1, ft_isascii('a'));
  TEST_ASSERT_EQUAL_INT(1, ft_isascii('A'));
  TEST_ASSERT_EQUAL_INT(1, ft_isascii('~'));
  TEST_ASSERT_EQUAL_INT(1, ft_isascii('!'));
  TEST_ASSERT_EQUAL_INT(1, ft_isascii(0));
  TEST_ASSERT_EQUAL_INT(1, ft_isascii(127));
  TEST_ASSERT_EQUAL_INT(0, ft_isascii(-1));
  TEST_ASSERT_EQUAL_INT(1, ft_isascii(0.0));
  TEST_ASSERT_EQUAL_INT(0, ft_isascii(128));
}

void test_isprint(void) {
  TEST_ASSERT_EQUAL_INT(1, ft_isprint('1'));
  TEST_ASSERT_EQUAL_INT(1, ft_isprint('a'));
  TEST_ASSERT_EQUAL_INT(1, ft_isprint('A'));
  TEST_ASSERT_EQUAL_INT(1, ft_isprint('~'));
  TEST_ASSERT_EQUAL_INT(1, ft_isprint('!'));
  TEST_ASSERT_EQUAL_INT(1, ft_isprint(' '));
  TEST_ASSERT_EQUAL_INT(0, ft_isprint(19));
  TEST_ASSERT_EQUAL_INT(0, ft_isprint(0));
  TEST_ASSERT_EQUAL_INT(0, ft_isprint(127));
  TEST_ASSERT_EQUAL_INT(0, ft_isprint(-1));
  TEST_ASSERT_EQUAL_INT(0, ft_isprint(0.0));
}

void test_isalpha(void) {
  TEST_ASSERT_EQUAL_INT(0, ft_isalpha('1'));
  TEST_ASSERT_EQUAL_INT(1, ft_isalpha('a'));
  TEST_ASSERT_EQUAL_INT(1, ft_isalpha('A'));
  TEST_ASSERT_EQUAL_INT(0, ft_isalpha('~'));
  TEST_ASSERT_EQUAL_INT(0, ft_isalpha('!'));
}

void test_isdigit(void) {
  TEST_ASSERT_EQUAL_INT(1, ft_isdigit('1'));
  TEST_ASSERT_EQUAL_INT(0, ft_isdigit('a'));
  TEST_ASSERT_EQUAL_INT(0, ft_isdigit('A'));
  TEST_ASSERT_EQUAL_INT(0, ft_isdigit('~'));
  TEST_ASSERT_EQUAL_INT(0, ft_isdigit('!'));
}

void test_isalnum(void) {
  TEST_ASSERT_EQUAL_INT(1, ft_isalnum('1'));
  TEST_ASSERT_EQUAL_INT(1, ft_isalnum('a'));
  TEST_ASSERT_EQUAL_INT(1, ft_isalnum('A'));
  TEST_ASSERT_EQUAL_INT(0, ft_isalnum('~'));
  TEST_ASSERT_EQUAL_INT(0, ft_isalnum('!'));
}

void test_tolower(void) {
  TEST_ASSERT_EQUAL_CHAR('1', ft_tolower('1'));
  TEST_ASSERT_EQUAL_CHAR('a', ft_tolower('a'));
  TEST_ASSERT_EQUAL_CHAR('a', ft_tolower('A'));
  TEST_ASSERT_EQUAL_CHAR('~', ft_tolower('~'));
  TEST_ASSERT_EQUAL_INT(0, ft_tolower(0));
  TEST_ASSERT_EQUAL_INT(127, ft_tolower(127));
  TEST_ASSERT_EQUAL_CHAR('z', ft_tolower('Z'));
  TEST_ASSERT_EQUAL_INT(0, ft_tolower(0.0));
}

void test_toupper(void) {
  TEST_ASSERT_EQUAL_CHAR('1', ft_toupper('1'));
  TEST_ASSERT_EQUAL_CHAR('A', ft_toupper('a'));
  TEST_ASSERT_EQUAL_CHAR('A', ft_toupper('A'));
  TEST_ASSERT_EQUAL_CHAR('~', ft_toupper('~'));
  TEST_ASSERT_EQUAL_INT(0, ft_toupper(0));
  TEST_ASSERT_EQUAL_INT(127, ft_toupper(127));
  TEST_ASSERT_EQUAL_CHAR('Z', ft_toupper('z'));
  TEST_ASSERT_EQUAL_INT(0, ft_toupper(0.0));
}

void test_atoi(void) {
  TEST_ASSERT_EQUAL_INT32(0, ft_atoi("0"));
  TEST_ASSERT_EQUAL_INT32(10, ft_atoi("10"));
  TEST_ASSERT_EQUAL_INT32(10, ft_atoi("+10"));
  TEST_ASSERT_EQUAL_INT32(-10, ft_atoi("-10"));
  TEST_ASSERT_EQUAL_INT32(0, ft_atoi("0.0"));
  TEST_ASSERT_EQUAL_INT32(100000, ft_atoi("100000.1001232123123123"));
  TEST_ASSERT_EQUAL_INT32(123456789, ft_atoi("123456789"));
  TEST_ASSERT_EQUAL_INT32(2147483647, ft_atoi("2147483647"));
  TEST_ASSERT_EQUAL_INT32(-2147483648, ft_atoi("-2147483648"));
}

void test_strlen(void) {
  TEST_ASSERT_EQUAL_INT(0, ft_strlen(""));
  TEST_ASSERT_EQUAL_INT(1, ft_strlen("0"));
  TEST_ASSERT_EQUAL_INT(3, ft_strlen("+10"));
  TEST_ASSERT_EQUAL_INT(4, ft_strlen("~-10"));
  TEST_ASSERT_EQUAL_INT(3, ft_strlen("0.0"));
  TEST_ASSERT_EQUAL_INT(23, ft_strlen("100000.1001232123123123"));
  TEST_ASSERT_EQUAL_INT(9, ft_strlen("123456789"));
}

void test_strchr(void) {
  const char *example = "abcdefghijklmnopqrstuvwxyz123456~ [>.,";
  TEST_ASSERT_EQUAL_STRING(NULL, ft_strchr(example, '?'));
  TEST_ASSERT_EQUAL_STRING(example, ft_strchr(example, 'a'));
  TEST_ASSERT_EQUAL_STRING(example + 2, ft_strchr(example, 'c'));
  TEST_ASSERT_EQUAL_STRING(example + 32, ft_strchr(example, '~'));
  TEST_ASSERT_EQUAL_STRING(example + 36, ft_strchr(example, '.'));
  TEST_ASSERT_EQUAL_STRING(example + 37, ft_strchr(example, ','));
}

void test_strrchr(void) {
  const char *example = "abcdefghijklmnopqrstuvwxyz123456~ [>.c,";
  TEST_ASSERT_EQUAL_STRING(NULL, ft_strrchr(example, '?'));
  TEST_ASSERT_EQUAL_STRING(example + 37, ft_strrchr(example, 'c'));
  TEST_ASSERT_EQUAL_STRING(example, ft_strrchr(example, 'a'));
  TEST_ASSERT_EQUAL_STRING(example + 32, ft_strrchr(example, '~'));
  TEST_ASSERT_EQUAL_STRING(example + 36, ft_strrchr(example, '.'));
  TEST_ASSERT_EQUAL_STRING(example + 38, ft_strrchr(example, ','));
}

void test_strnstr(void) {
  const char *example = "abcdefghijklmnopqrstuvwxyz123456~ [>.c,";
  TEST_ASSERT_EQUAL_STRING(example, ft_strnstr(example, "abc", 3));
  TEST_ASSERT_EQUAL_STRING(NULL, ft_strnstr(example, "abc", 0));
  TEST_ASSERT_EQUAL_STRING(NULL, ft_strnstr(example, "abc", 2));
  TEST_ASSERT_EQUAL_STRING(example + 2, ft_strnstr(example, "c", 4));
  TEST_ASSERT_EQUAL_STRING(example + 32, ft_strnstr(example, "~ ", 34));
}

void test_memchr(void) {
  const char *example = "abcdefghijklmnopqrstuvwxyz123456~ [>.,";
  const char *example2 = "?";
  TEST_ASSERT_EQUAL_STRING(NULL, ft_memchr(example, '+', 100));
  TEST_ASSERT_EQUAL_STRING(example, ft_memchr(example, 'a', 1));
  TEST_ASSERT_EQUAL_STRING(example + 2, ft_memchr(example, 'c', 3));
  TEST_ASSERT_EQUAL_STRING(example + 32, ft_memchr(example, '~', 33));
  TEST_ASSERT_EQUAL_STRING(example + 36, ft_memchr(example, '.', 37));
  TEST_ASSERT_EQUAL_STRING(example + 37, ft_memchr(example, ',', 38));
  TEST_ASSERT_EQUAL_STRING(NULL, ft_memchr(example2, '?', 0));
}

void test_memset(void) {
  char example[] = "abcde";
  TEST_ASSERT_EQUAL_STRING("abcde", example);
  ft_memset(example, '0', 1);
  TEST_ASSERT_EQUAL_STRING("0bcde", example);
  ft_memset(example + 4, '0', 1);
  TEST_ASSERT_EQUAL_STRING("0bcd0", example);
  ft_memset(example + 2, '0', 2);
  TEST_ASSERT_EQUAL_STRING("0b000", example);
  ft_memset(example, '~', 5);
  TEST_ASSERT_EQUAL_STRING("~~~~~", example);
}

void test_bzero(void) {
  char str[] = "abcde";
  ft_bzero(str, 1);
  TEST_ASSERT_EQUAL_STRING("\0", str);
  TEST_ASSERT_EQUAL_STRING("bcde", str + 1);
  ft_bzero(str + 1, 1);
  TEST_ASSERT_EQUAL_STRING("\0", str + 1);
  TEST_ASSERT_EQUAL_STRING("cde", str + 2);
  ft_bzero(str, 5);
  TEST_ASSERT_EQUAL_STRING("\0", str);
  TEST_ASSERT_EQUAL_STRING("\0", str + 1);
  TEST_ASSERT_EQUAL_STRING("\0", str + 2);
  TEST_ASSERT_EQUAL_STRING("\0", str + 3);
  TEST_ASSERT_EQUAL_STRING("\0", str + 4);
  ft_bzero(str, 6);
  TEST_ASSERT_EQUAL_STRING("\0", str + 5);
}

void test_memcpy(void) {
  char source[] = "0123456789";
  char dest[] = "0000000000";
  ft_memcpy(dest, source, 0);
  TEST_ASSERT_EQUAL_STRING("0000000000", dest);
  ft_memcpy(dest, source, 1);
  TEST_ASSERT_EQUAL_STRING("0000000000", dest);
  ft_memcpy(dest + 9, source + 9, 1);
  TEST_ASSERT_EQUAL_STRING("0000000009", dest);
  ft_memcpy(dest, source, 10);
  TEST_ASSERT_EQUAL_STRING("0123456789", dest);
}

void test_strncmp(void) {
  char str1[] = "abcdefgh";
  char str2[] = "abcdefgh";
  TEST_ASSERT_EQUAL_INT(0, ft_strncmp(str1, str2, 100));
  char str3[] = "abceefgh";
  char str4[] = "abcdefgh";
  TEST_ASSERT_EQUAL_INT(1, ft_strncmp(str3, str4, 4));
  char str5[] = "";
  char str6[] = "";
  TEST_ASSERT_EQUAL_INT(0, ft_strncmp(str5, str6, 0));
  char str7[] = "a";
  char str8[] = "b";
  TEST_ASSERT_EQUAL_INT(-1, ft_strncmp(str7, str8, 1));
}

void test_memcmp(void) {
  char str1[] = "abcdefgh";
  char str2[] = "abcdefgh";
  TEST_ASSERT_EQUAL_INT(0, ft_memcmp(str1, str2, 9));
  TEST_ASSERT_NOT_EQUAL_INT(0, ft_memcmp(str1, str2, 100));
  char str3[] = "abceefgh";
  char str4[] = "abcdefgh";
  TEST_ASSERT_EQUAL_INT(1, ft_memcmp(str3, str4, 4));
  char str5[] = "";
  char str6[] = "";
  TEST_ASSERT_EQUAL_INT(0, ft_memcmp(str5, str6, 0));
  char str7[] = "a";
  char str8[] = "b";
  TEST_ASSERT_EQUAL_INT(-1, ft_memcmp(str7, str8, 1));
}

void test_strlcpy(void) {
  const char source[4] = "012";
  char dest[] = "";
  ft_strlcpy(dest, source, 4);
  TEST_ASSERT_EQUAL_STRING("012", dest);
  ft_strlcpy(dest, source, 0);
  TEST_ASSERT_EQUAL_STRING("012", dest);
  const char source1[4] = "012";
  ft_strlcpy(dest, source1, 10);
  TEST_ASSERT_EQUAL_STRING("012", dest);
}

void test_strlcat(void) {
  char dest[10] = "";
  ft_strlcat(dest, "012", 4);
  TEST_ASSERT_EQUAL_STRING("012", dest);
  ft_strlcat(dest, "345", 7);
  TEST_ASSERT_EQUAL_STRING("012345", dest);
  ft_strlcat(dest, "6", 10);
  TEST_ASSERT_EQUAL_STRING("0123456", dest);
  ft_strlcat(dest, "7", 0);
  TEST_ASSERT_EQUAL_STRING("0123456", dest);

	char * dest1 = (char *)malloc(sizeof(char) * 15);
	memset(dest1, 0, 15);
	memset(dest1, 'r', 6);
	dest1[10] = 'a';
	ft_strlcat(dest1, "lorem ipsum dolor sit amet", 0);
	TEST_ASSERT_EQUAL_STRING("rrrrrr", dest1);
	free(dest1);
	dest1 = NULL;
}

void test_strdup(void) {
  char *dest = ft_strdup("012");
  TEST_ASSERT_EQUAL_STRING("012", dest);
  dest = ft_strdup("345");
  TEST_ASSERT_EQUAL_STRING("345", dest);
  dest = ft_strdup("6");
  TEST_ASSERT_EQUAL_STRING("6", dest);
  dest = ft_strdup("7");
  TEST_ASSERT_EQUAL_STRING("7", dest);
  free(dest);
  dest = NULL;
}

void test_memmove(void) {
  char *src1 = ft_strdup("zxc");
  char dest[] = "abc";
  char *src = ft_strdup("cde");
  ft_memmove(dest, src, 3);
  free(src);
  src = NULL;
  TEST_ASSERT_EQUAL_STRING("cde", dest);
  ft_memmove(dest, src1, 2);
  TEST_ASSERT_EQUAL_STRING("zxe", dest);
  free(src1);
  src1 = NULL;
}

void test_calloc(void) {
  char *dest = ft_calloc(3, sizeof(char));
  TEST_ASSERT_EQUAL_INT(sizeof(char *), sizeof(dest));
  TEST_ASSERT_EQUAL_CHAR('\0', dest[0]);
  TEST_ASSERT_EQUAL_CHAR('\0', dest[1]);
  TEST_ASSERT_EQUAL_CHAR('\0', dest[2]);

  int *arr = ft_calloc(1, sizeof(int));
  TEST_ASSERT_EQUAL_INT(0, arr[0]);
	
	//Looks like this test depends on compiler realisation
  //int *error = ft_calloc(0, sizeof(int));
  //TEST_ASSERT_NOT_EQUAL_UINT('\0', error[0]);

  free(dest);
  dest = NULL;
  free(arr);
  arr = NULL;
  //free(error);
  //error = NULL;
}

// Part II
void test_substr(void) {
	int size = 5;
  char * dest = ft_calloc(size, sizeof(char));
	dest = ft_substr("abcdefghi", 0, 5);
	TEST_ASSERT_EQUAL_STRING("abcde", dest);
	dest = ft_substr("abcdefghi", 1, 4);	
	TEST_ASSERT_EQUAL_STRING("bcde", dest);
	dest = ft_substr(NULL, 0, 0);
	TEST_ASSERT_EQUAL_STRING(NULL, dest);
	dest = ft_substr("12345", 2, 100);
	TEST_ASSERT_EQUAL_STRING("345", dest);
	
	char lorem[] = "lorem ipsum dolor sit amet";
	char *substr;
	substr = ft_substr(lorem, 0, 10);
	TEST_ASSERT_EQUAL_STRING("lorem ipsu", substr);
	
	substr = ft_substr(lorem, 7, 10);
	TEST_ASSERT_EQUAL_STRING("psum dolor", substr);

	free(dest);
	dest = NULL;
	free(substr);
	substr = NULL;
}

void test_strjoin(void) {
	int size = 10;
  char * dest = ft_calloc(size, sizeof(char));
	dest = ft_strjoin("abcde", "fghij");
	TEST_ASSERT_EQUAL_STRING("abcdefghij", dest);
	dest = ft_strjoin(NULL, "abcd");
	TEST_ASSERT_EQUAL_STRING(NULL, dest);
	dest = ft_strjoin("abcd", NULL);
	TEST_ASSERT_EQUAL_STRING(NULL, dest);
	dest = ft_strjoin("", "");
	TEST_ASSERT_EQUAL_STRING("", dest);
	dest = ft_strjoin("", "a");
  TEST_ASSERT_EQUAL_STRING("a", dest);
	free(dest);
	dest = NULL;
}

void test_strtrim(void) {
	int size = 10;
  char * dest = ft_calloc(size, sizeof(char));
	dest = ft_strtrim("abcdeia", "a");
	TEST_ASSERT_EQUAL_STRING("bcdei", dest);
	dest = ft_strtrim(NULL, "abcd");
	TEST_ASSERT_EQUAL_STRING(NULL, dest);
	dest = ft_strtrim("abcd", NULL);
	TEST_ASSERT_EQUAL_STRING(NULL, dest);
	dest = ft_strtrim("", "");
	TEST_ASSERT_EQUAL_STRING("", dest);
	dest = ft_strtrim("aa", "a");
  TEST_ASSERT_EQUAL_STRING("", dest);
	dest = ft_strtrim("abc12abc3bca", "abc");
	TEST_ASSERT_EQUAL_STRING("12abc3", dest);
	free(dest);
	dest=NULL;
}

void test_itoa(void) {
  char * dest = ft_calloc(10, sizeof(char));
	dest = ft_itoa(0);
	TEST_ASSERT_EQUAL_STRING("0", dest);
	dest = ft_itoa(1);
	TEST_ASSERT_EQUAL_STRING("1", dest);
	dest = ft_itoa(-1);
	TEST_ASSERT_EQUAL_STRING("-1", dest);
	dest = ft_itoa(-2147483648);
	TEST_ASSERT_EQUAL_STRING("-2147483648", dest);
	dest = ft_itoa(2147483647);
  TEST_ASSERT_EQUAL_STRING("2147483647", dest);
	free(dest);
	dest = NULL;
}

void test_split(void){
	char ** result = ft_split("acaca", 'c');
	//for(int i = 0; result[i] != NULL; i++){
	//	puts(result[i]);
	//}
	TEST_ASSERT_EQUAL_STRING("a", result[0]);
	TEST_ASSERT_EQUAL_STRING("a", result[1]);
	TEST_ASSERT_EQUAL_STRING("a", result[2]);
	TEST_ASSERT_EQUAL_STRING(NULL, result[3]);

	result = ft_split("aca", 'b');
	TEST_ASSERT_EQUAL_STRING(result[0], "aca");
	TEST_ASSERT_EQUAL_STRING(result[1], NULL);
	
	result = ft_split("acad~acad", '~');
	TEST_ASSERT_EQUAL_STRING("acad", result[0]);
	TEST_ASSERT_EQUAL_STRING("acad", result[1]);

	result = ft_split("", 'a');
	TEST_ASSERT_EQUAL_STRING(NULL, result[0]);

	result = ft_split("   lorem   ipsum dolor     sit amet, consectetur   adipiscing elit. Sed non risus. Suspendisse   ", 'i');
	for(int i = 0; result[i] != NULL; i++){
		printf("|%s|\n", result[i]);
	}
}

char test_upper(unsigned int i, char c){
	
	char result = '\0';

	if (i % 2) {
		result = (char)ft_toupper(c);
	} else {
		result = (char)ft_tolower(c);
	}

	return result;
}

void test_mapi(void){
	char * result = ft_strmapi("aBcDe", &test_upper);
	TEST_ASSERT_EQUAL_STRING("aBcDe", result);
	
	result = ft_strmapi("", &test_upper);
	TEST_ASSERT_EQUAL_STRING("", result);
	
	free(result);
	result = NULL;
}

void test_it(unsigned int i, char * c){
	if (i % 2){
		*c += 32;
	} else {
		*c -= 32;
	}
}

void test_iteri(void){
	char * result = (char *)calloc(10, sizeof(char));
	ft_strlcpy(result, "aBcDe", 6);

	ft_striteri(result, &test_it);
	TEST_ASSERT_EQUAL_STRING("AbCdE", result);

	ft_strlcpy(result, "", 1);
	ft_striteri(result, &test_it);
	TEST_ASSERT_EQUAL_STRING("", result);
	free(result);
	result = NULL;
}

void test_putchar(void){

	int fd = open("test.txt", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);

	ft_putchar_fd('a', fd);
	lseek(fd, 0, SEEK_SET);
	
	char ch;

	read(fd, &ch, 1);

	TEST_ASSERT_EQUAL_INT('a', ch);

	lseek(fd, 0, SEEK_SET);
	ft_putchar_fd('1', fd);
	lseek(fd, 0, SEEK_SET);
  
	read(fd, &ch, 1);

	TEST_ASSERT_EQUAL_INT('1', ch);
	
	lseek(fd, 0, SEEK_SET);
	ft_putchar_fd('\0', fd);
	lseek(fd, 0, SEEK_SET);
  
	read(fd, &ch, 1);

	TEST_ASSERT_EQUAL_INT('\0', ch);

	close(fd);
	unlink("test.txt");
}

void test_putnbr(void){

	int fd = open("test.txt", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);

	ft_putnbr_fd(-1000000, fd);
	lseek(fd, 0, SEEK_SET);
	
	char buff[16];

	read(fd, buff, 8);
	buff[8] = '\0';
	TEST_ASSERT_EQUAL_STRING("-1000000", buff);

	lseek(fd, 0, SEEK_SET);
	ft_putnbr_fd(INT_MAX, fd);
	lseek(fd, 0, SEEK_SET);
  
	read(fd, buff, 10);
	buff[10] = '\0';
	TEST_ASSERT_EQUAL_STRING(ft_itoa(INT_MAX), buff);
	
	lseek(fd, 0, SEEK_SET);
	ft_putnbr_fd(INT_MIN, fd);
	lseek(fd, 0, SEEK_SET);
	
	read(fd, buff, 11);
	buff[11] = '\0';
	TEST_ASSERT_EQUAL_STRING(ft_itoa(INT_MIN), buff);

	close(fd);
	unlink("test.txt");
}

void test_putstr(void){

	int fd = open("test.txt", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);

	ft_putstr_fd("abc", fd);
	lseek(fd, 0, SEEK_SET);
	
	char buff[16];

	read(fd, buff, 3);
	buff[3] = '\0';
	TEST_ASSERT_EQUAL_STRING("abc", buff);

	lseek(fd, 0, SEEK_SET);
	ft_putstr_fd("abcdefghijklmno", fd);
	lseek(fd, 0, SEEK_SET);
  
	read(fd, buff, 16);
	buff[15] = '\0';
	TEST_ASSERT_EQUAL_STRING("abcdefghijklmno", buff);

	close(fd);
	unlink("test.txt");
}

void test_putendl(void){

	int fd = open("test.txt", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);

	ft_putendl_fd("abc", fd);
	lseek(fd, 0, SEEK_SET);
	
	char buff[16];

	read(fd, buff, 5);
	TEST_ASSERT_EQUAL_INT('\n', buff[3]);

	lseek(fd, 0, SEEK_SET);
	ft_putendl_fd("abcdefghijklmn", fd);
	lseek(fd, 0, SEEK_SET);
  
	read(fd, buff, 16);
	buff[15] = '\0';	
	TEST_ASSERT_EQUAL_STRING("abcdefghijklmn\n", buff);

	close(fd);
	unlink("test.txt");
}            

//BONUS PART

void test_lstnew(void){
	t_list * new_list = (void *)ft_lstnew("Hello world");

	TEST_ASSERT_EQUAL_STRING("Hello world", new_list->content);
	TEST_ASSERT_EQUAL_STRING(NULL, new_list->next);

	free(new_list);
	new_list = NULL;
}




int main(void) {
  UnityBegin("test.c");
  puts("Part I");
	RUN_TEST(test_isprint);
  RUN_TEST(test_isascii);
  RUN_TEST(test_isalpha);
  RUN_TEST(test_isdigit);
  RUN_TEST(test_isalnum);
  RUN_TEST(test_tolower);
  RUN_TEST(test_toupper);
  RUN_TEST(test_atoi);
  RUN_TEST(test_strlen);
  RUN_TEST(test_strchr);
  RUN_TEST(test_strrchr);
  RUN_TEST(test_strnstr);
  RUN_TEST(test_memchr);
  RUN_TEST(test_memset);
  RUN_TEST(test_bzero);
  RUN_TEST(test_memcpy);
  RUN_TEST(test_strncmp);
  RUN_TEST(test_memcmp);
  RUN_TEST(test_strlcpy);
  RUN_TEST(test_strlcat);
  RUN_TEST(test_strdup);
  RUN_TEST(test_memmove);
  RUN_TEST(test_calloc);
	
	puts("Part II");
	RUN_TEST(test_substr);
	RUN_TEST(test_strjoin);
	RUN_TEST(test_strtrim);
	RUN_TEST(test_itoa);
	RUN_TEST(test_split);
	RUN_TEST(test_mapi);
	RUN_TEST(test_iteri);
	RUN_TEST(test_putchar);
	RUN_TEST(test_putnbr);
	RUN_TEST(test_putstr);
  RUN_TEST(test_putendl);
	
	puts("BONUS");
	RUN_TEST(test_lstnew);

	return UnityEnd();
}

// RUN_TEST(test_strcmp);
// RUN_TEST(test_strcpy);
// RUN_TEST(test_strncpy);
/*
void test_strcpy(void){
  char str1[10] = "abcde";
        char str2[10];
        ft_strcpy(str2, str1);
        TEST_ASSERT_EQUAL_STRING("abcde", str2);
        ft_strcpy(str2, "0000000000");
        TEST_ASSERT_EQUAL_STRING("0000000000", str2);
        ft_strcpy(str2, "");
        TEST_ASSERT_EQUAL_STRING("", str2);
}

void test_strncpy(void){
  char str1[10] = "abcde";
        char str2[10];
        ft_strncpy(str2, str1, 6);
        TEST_ASSERT_EQUAL_STRING("abcde", str2);
        ft_strncpy(str2, "0000000000", 1);
        TEST_ASSERT_EQUAL_STRING("0", str2);
        ft_strncpy(str2, "", 0);
        TEST_ASSERT_EQUAL_STRING("", str2);
}
*/
/*
void test_strcmp(void){
        char str1[] = "abcdefgh";
  char str2[] = "abcdefgh";
        TEST_ASSERT_EQUAL_INT(0, ft_strcmp(str1, str2));
        char str3[] = "abceefgh";
  char str4[] = "abcdefgh";
        TEST_ASSERT_EQUAL_INT(1, ft_strcmp(str3, str4));
        char str5[] = "";
        char str6[] = "";
        TEST_ASSERT_EQUAL_INT(0, ft_strcmp(str5, str6));
        char str7[] = "a";
        char str8[] = "b";
        TEST_ASSERT_EQUAL_INT(-1, ft_strcmp(str7, str8));
}
*/
