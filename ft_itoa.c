/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oparilov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/25 19:46:45 by oparilov          #+#    #+#             */
/*   Updated: 2023/01/28 18:17:55 by oleksii          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_ilen(int num)
{
	int	len;
	int	n;

	if (num == 0)
	{
		return (1);
	}
	len = 0;
	n = num;
	if (n < 0)
	{
		len++;
		n *= -1;
	}
	while (n != 0)
	{
		len++;
		n /= 10;
	}
	return (len);
}

char	*ft_itoa(int n)
{
	int		sign;
	int		n_len;
	char	*result;

	sign = 1;
	n_len = ft_ilen(n);
	result = ft_calloc((n_len + 1), sizeof(char));
	if (!result)
		return (NULL);
	if (n < 0)
		sign = -1;
	result[n_len] = '\0';
	n_len--;
	while (n_len >= 0)
	{
		result[n_len] = (n % 10) * sign + '0';
		n /= 10;
		n_len--;
	}
	if (sign == -1)
		result[0] = '-';
	return (result);
}
