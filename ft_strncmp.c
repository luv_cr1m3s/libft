/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oparilov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/25 19:49:39 by oparilov          #+#    #+#             */
/*   Updated: 2023/01/28 18:16:35 by oleksii          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *str1, const char *str2, size_t n)
{
	size_t	i;

	if (n == 0)
	{
		return (0);
	}
	if (!str1 && !str2)
	{
		return (0);
	}
	i = 0;
	while (str1[i] != '\0' && str1[i] == str2[i] && (i < n - 1))
		i++;
	return ((unsigned char)str1[i] - (unsigned char)str2[i]);
}
