/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oparilov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/25 19:49:32 by oparilov          #+#    #+#             */
/*   Updated: 2023/01/25 19:49:33 by oparilov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *str, int ch)
{
	int	length;

	if (!str || (ch > 255) || (ch < 0))
	{
		return (NULL);
	}
	if (ch == '\0')
	{
		return (ft_strchr(str, ch));
	}
	length = ft_strlen(str) - 1;
	while (length >= 0)
	{
		if ((char)ch == str[length])
		{
			return ((char *)&str[length]);
		}
		length--;
	}
	return (NULL);
}
